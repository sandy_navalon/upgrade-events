/* **Iteración #1: Creando Events** */
'use strict'

window.onload = () => {
/* Dado el siguiente HTML: */

/* 1.1 Añade un botón a tu html con el id btnToClick y en tu javascript añade el
evento click que ejecute un console log con la información del evento del click */

const button = document.createElement('button');
button.id = 'btnToClick';
button.innerHTML ='Click here!';

document.body.appendChild(button);


var toClick = function() {
    console.log('ay!');
 }
 document.getElementById("btnToClick").addEventListener("click", toClick);

/* 1.2 Añade un evento 'focus' que ejecute un console.log con el valor del input. */

const focus = () => {

    const btn2 = document.querySelector('.focus');

    let addClick = () => {
        console.log(btn2.value);
    }

    btn2.addEventListener('focus', addClick);

}
focus()

/* 1.3 Añade un evento 'input' que ejecute un console.log con el valor del input.*/

const input = () => {

    const btn3 = document.querySelector('.value');

    let addInput = () => {
        console.log(btn3.value);
    }

    btn3.addEventListener('input', addInput);

}

input()

}